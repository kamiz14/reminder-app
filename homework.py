from flask import Flask, render_template, request
from pony.orm import Database, select, db_session, Required

app = Flask(__name__)
db = Database()

class User(db.Entity):
    name = Required(str)
    due_date = Required(str)
    classes = Required(str)
    notes = Required(str)
    reminder = Required(str)

db.bind(provider='sqlite', filename='mydb')
db.generate_mapping()

@app.route('/')
def index():
    return render_template('homework.html')
@db_session
def add():
    if request.method == 'GET':
        return render_template('homework.html')

    elif request.method == 'POST':
        n = request.form.get('name', 'some default value')
        d = request.form.get('due date')
        c = request.form.get('class')
        o = request.form.get('notes')
        r = request.form.get('reminder time')
        User(name=n, due_date=d, classes=c, notes=o,reminder_time=r)
        return f'lorem'

@app.route('/table')
@db_session
def table():

    homework = list(select(n for n in User if n.name[0] == 'f'))
    homework = list(select(d for d in User if d.due_date[0] == 'f'))
    homework = list(select(c for c in User if c.classes[0]=='f'))
    homework = list(select(o for o in User if o.notes[0] == 'f'))
    homework = list(select(r for r in User if r.reminder_time[0] == 'f'))
    return render_template('table.html', HOMEWORK=homework)