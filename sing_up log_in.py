from flask import Flask, render_template, request
from pony.orm import Database, select, db_session, Required

app = Flask(__name__)
db = Database()

class User(db.Entity):
    username = Required(str)
    password = Required(str)
    email = Required(str)

db.bind(provider='sqlite', filename='mydb')
db.generate_mapping()

class Person:
    def __init__(self,age,grade):
        self.age = age
        self.grade=grade

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/login', methods=['GET', 'POST'])
@db_session
def login():
    if request.method == 'GET':
        return render_template('index.html')

    elif request.method == 'POST':
        u = request.form.get('username', 'some default value')
        p = request.form.get('password')
        e = request.form.get('email')
        a = request.form.get('age')
        g = request.form.get('grade')
        User(username=u, password=p, email=e)
        Person(age=a, grade=g)
        return f'Hello {u}, your password is {p} and your email is {e}'

@app.route('/table')
@db_session
def table():

    people = list(select(u for u in User if u.username[0] == 'f'))
    people = list(select(e for e in User if e.email[0] == 'f'))
    people = list(select(a for a in Person if a.age[0]=='f'))
    people = list(select(g for g in Person if g.grade[0] == 'f'))
    return render_template('table.html', PEOPLE=people)

